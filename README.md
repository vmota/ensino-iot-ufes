![logo do projeto](imgs/logo-gitlab.png)


# Projeto de Ensino 2019-2020 - IoT UFES

Este projeto aborda a temática de Internet das Coisas (Internet of Things – IoT), a qual consiste em fazer com que  “objetos” inteligentes comuniquem-se entre si e tomem decisões sem a necessidade de intervenção humana.

Este projeto visa a elaboração e disponibilização de projetos e material técnico didático sobre a temática Internet das coisas. O objetivo é desenvolver diversos projetos IoT completos de tal forma que professores de disciplinas correlatas possam utilizar os projetos sem se preocuparem com a parte não relacionada da disciplina. Por exemplo, considere um projeto IoT de sensoriamento de temperatura com exibição dos dados coletados na web. A disciplina de desenvolvimento Web poderia apresentar trabalhos práticos que envolvam a visualização dos dados na web sem se preocupar com a parte de sistemas embarcados (o sensoriamento). Assim como a disciplina de sistemas embarcados poderia apresentar os conceitos de obtenção de dados de sensores e não se preocupar com a visualização web.

Portanto, foram desenvolvidos projetos IoT com uma documentação de código aberto detalhada, de fácil entendimento e projetados de forma extensiva. Assim, permite-se que os interessados possam usufruir dos projetos e propor trabalhos, projetos de disciplinas ou mesmo monografias abordando a temática de IoT, abstraindo detalhes que sejam alheios às disciplinas.

**Dependências:**

Excetuando o projeto 1 que é só uma introdução ao ESP8266, todos os outros projetos envolvem upload de código, logo necessitam ter instalado o Arduino IDE que é utilizado para fazer o upload dos códigos e de um pacote de gerenciador de placas para identificar no Arduino IDE que a placa utilizada possui um ESP8266.

[Aqui](https://gitlab.com/vmota/ensino-iot-ufes/snippets/1873569) temos um breve roteiro de como instalar o pacote na IDE.


**Projetos utilizando o ESP8266:**
  
**Básico:**

**Nível 1/5:**
1. [Conhecendo o ESP8266](https://gitlab.com/vmota/ensino-iot-ufes/snippets/1863173)

**Nível 2/5:**
1. [Piscando o LED do Módulo ESP8266](https://gitlab.com/vmota/ensino-iot-ufes/snippets/1858507)
2. [Medindo umidade e temperatura com o sensor DHT11 ](https://gitlab.com/vmota/ensino-iot-ufes/snippets/1864276)
3. [Medindo umidade do solo com higrômetro](https://gitlab.com/vmota/ensino-iot-ufes/snippets/1900519)   
4. [Medindo vazão de água com sensor YF-S201](https://gitlab.com/vmota/ensino-iot-ufes/snippets/1880678)   
5. [Utilizando um Micro Servo 9g com o ESP8266](https://gitlab.com/vmota/ensino-iot-ufes/snippets/1886792)
6. [Sensor de som HW-484](https://gitlab.com/vmota/ensino-iot-ufes/snippets/2019608)

**Nível 3/5:**
1. [Utilizando um sensor GPS com o ESP8266 ](https://gitlab.com/vmota/ensino-iot-ufes/snippets/1864541)
2. [Medindo a qualidade do ar com um DSM501A](https://gitlab.com/vmota/ensino-iot-ufes/snippets/1891189)

 
**Sensores I2C:**

**Nível 2/5:**
1. [Sensor de presença PIR HC-SR501](https://gitlab.com/vmota/ensino-iot-ufes/snippets/1882411)
2. [Medindo umidade, temperatura e pressão com o sensor BME280 ](https://gitlab.com/vmota/ensino-iot-ufes/snippets/1876375)
3. [Medindo índice UV com sensor CJMCU6750](https://gitlab.com/vmota/ensino-iot-ufes/snippets/1878277)
4. [Medindo a aceleração com o sensor MPU6050](https://gitlab.com/vmota/ensino-iot-ufes/snippets/1863178)
    
**Projetos:**

**Nivel 3/5:**
1. [Acendendo um LED via Web com o Node RED e MQTT](https://gitlab.com/vmota/ensino-iot-ufes/snippets/1856875)
2. [Enviando dados do sensor de temperatura e umidade para a internet com ThingSpeak](https://gitlab.com/vmota/ensino-iot-ufes/snippets/1898151)
3. [Enviando dados do sensor de UV para a internet com ThingSpeak e MQTT](https://gitlab.com/vmota/ensino-iot-ufes/snippets/1897141)
4. [Verificando vazamento de água em torneiras](https://gitlab.com/vmota/ensino-iot-ufes/snippets/1990790)

**Nivel 4/5:**
1. [Implementando Controle de Irrigação](https://gitlab.com/vmota/ensino-iot-ufes/snippets/1909174)
2. [Dados controle irrigação](https://gitlab.com/vmota/ensino-iot-ufes/snippets/1911961)


**IBM Cloud:**

**Nível 1/5:**
1. [Reconhecendo comandos de voz com o IBM Cloud](https://gitlab.com/vmota/ensino-iot-ufes/-/snippets/2061088)


**Projetos utilizando o ESP32:**

**Nível 5/5:**
1. [Implementando controle por voz](https://gitlab.com/vmota/ensino-iot-ufes/-/snippets/2057784)


**Projetos utilizando o Raspberry Pi:   
[Lista de Projetos](https://gitlab.com/snippets/1685586)**

Apresentação do projeto semelhante desenvolvido na UFOP em 2017:

[Internet das Coisas como plataforma multidisciplinar de aprendizagem - X Mostra Pró-Ativa](https://gitlab.com/IoT-UFOP/IoTu/blob/master/Internet%20das%20Coisas%20como%20plataforma%20multidisciplinar%20de%20aprendizagem%20-%20X%20Mostra%20Pr%C3%B3-Ativa.pdf)



_____

*Material produzido por meio do financiamento do Projeto de Ensino da [Universidade Federal do Espírito Santo](http://www.ufes.br) Edital 2019.*   
*Alunos responsáveis: Mikaella Ferreira, Renan Bottacine Amed Deud, Renato M. Machado;*   
*Coordenadores: [Vinícius F. S. Mota](https://inf.ufes.br/~vinicius.mota/) e Roberta Lima Gomes*   
*Trabalho sob a [licença Creative Commons 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/ "Creative Commons 4.0"), que permite copiar, distribuir e transmitir o trabalho em qualquer suporte ou formato desde que sejam citados a autoria e o licenciante. Está licença não permite o uso para fins comerciais e permite adaptações da obra desde que suas contribuições mantenham a mesma licença que o trabalho original.*

*<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.*
