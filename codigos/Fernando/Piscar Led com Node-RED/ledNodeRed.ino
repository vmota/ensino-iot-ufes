/*
  Código para acender e apagar o led interno usando uma pagina web com Node RED
*/

// the setup routine runs once when you press reset:
void setup() {
  // Inicializa o serial
  Serial.begin(9600);
  // Define o led interno como saida
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop routine runs over and over again forever:
void loop() {
  
  // Verifica se no buffer do serial existe algo
  if (Serial.available() > 0){
  // Armazena o bit do serial
  int codigo = Serial.parseInt();

  // Acende ou apaga o Led interno
  digitalWrite(LED_BUILTIN, codigo);
  
  }
}
