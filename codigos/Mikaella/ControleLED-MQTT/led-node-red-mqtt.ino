#include <ESP8266WiFi.h>
#include<PubSubClient.h>

const char* mqtt_server="192.168.0.116";
const char* ssid = "Development";
WiFiClient espclient;

void setup() {
  //Inicializa LED
  pinMode(LED_BUILTIN,OUTPUT);
  digitalWrite(LED_BUILTIN, HIGH);

  //Inicializa Serial
  Serial.begin(115200);

  //Conecta WiFi
  connectWifi();

}

void callback(char* topic,byte* message,unsigned int length1){

  Serial.print("Mensagem recebida de ");
  Serial.println(topic);

  int in = (int)message[0] - 48;

  if(!strcmp(topic, "ledcontrol"))
    digitalWrite(2, in);


}

PubSubClient client(mqtt_server,1883,callback,espclient);

void connectWifi(){
  WiFi.begin(ssid);

  Serial.println("\nConectando WiFi");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.print("\nWiFi conectado - ESP IP: ");
  Serial.println(WiFi.localIP());

}

//Conecta ao servidor mqtt
void reconnect(){
  while(!client.connected()){
    if(client.connect("ESP8266Client")){
      Serial.println("Conectado");
      client.subscribe("ledcontrol"); //receber dados de topic "ledcontrol"
    }
    else{
      Serial.print("failed,rc=");
      Serial.println(client.state());
      delay(500);
    }
  } 
}

void loop() {
    if(!client.connected())
      reconnect();
    
    //Se conectou, permite processar entrada
    if(!client.loop())
      reconnect();

}
