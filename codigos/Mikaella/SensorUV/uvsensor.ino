#include "Adafruit_VEML6070.h"

Adafruit_VEML6070 UVSensor = Adafruit_VEML6070();
float volt = 5.0;

void setup() {
  Serial.begin(115200); //Iniciando serial
  Serial.println("Iniciando medição UV");
  UVSensor.begin(VEML6070_1_T); //Iniciando o sensor
}

void loop() {
  Serial.print("UV level: "); 
  int uv = indiceUV(UVSensor.readUV()); //Obtem indice
  Serial.print(uv);
  char* nivel = nivelUV(uv); //Obtem nivel
  Serial.print(" - "); Serial.println(nivel);
  
  delay(1000);
}

//Pega o dado lido do sensor e interpreta em um indice
int indiceUV(float dado){
  int UV_index;
  float tensao = dado/1024.0*volt;

  if (tensao > 0 && tensao < 50)
  {
    UV_index = 0;
  }
  else if (tensao > 50 && tensao <= 227)
  {
    UV_index = 0;
  }
  else if (tensao > 227 && tensao <= 318)
  {
    UV_index = 1;
  }
  else if (tensao > 318 && tensao <= 408)
  {
    UV_index = 2;
  }
  else if (tensao > 408 && tensao <= 503)
  {
    UV_index = 3;
  }
  else if (tensao > 503 && tensao <= 606)
  {
    UV_index = 4;
  }
  else if (tensao > 606 && tensao <= 696)
  {
    UV_index = 5;
  }
  else if (tensao > 696 && tensao <= 795)
  {
    UV_index = 6;
  }
  else if (tensao > 795 && tensao <= 881)
  {
    UV_index = 7;
  }
  else if (tensao > 881 && tensao <= 976)
  {
    UV_index = 8;
  }
  else if (tensao > 976 && tensao <= 1079)
  {
    UV_index = 9;
  }
  else if (tensao > 1079 && tensao <= 1170)
  {
    UV_index = 10;
  }
  else if (tensao > 1170)
  {
    UV_index = 11;
  }

  return UV_index;
}

//Interpreta em nivel UV de acordo com o indice obtido
char* nivelUV(int indice){
  char* nivel;
  
  if(indice<= 2){
    nivel = "Baixo";
  } else if (indice<= 5){
    nivel = "Moderado";
  } else if (indice<= 7){
    nivel = "Alto";
  } else if (indice<= 10){
    nivel = "Altissimo";
  } else {
    nivel = "Extremo";
  }

  return nivel;
}
