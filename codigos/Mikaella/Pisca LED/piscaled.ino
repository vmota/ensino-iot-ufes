void setup() {
  pinMode(LED_BUILTIN, OUTPUT); //Inicializa LED
}

void loop() {
  digitalWrite(LED_BUILTIN, LOW); //Acende o LED
  delay(1000);                       //Espera 1 seg
  digitalWrite(LED_BUILTIN, HIGH);    // Apaga o LED
  delay(1000);                    //Espera 1 seg
}
