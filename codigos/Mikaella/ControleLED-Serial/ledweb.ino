void setup() {
  Serial.begin(115200);  //Inicializa a serial

  pinMode(LED_BUILTIN, OUTPUT); //Inicializa o led

}

void loop() {
  if (Serial.available()) { 
    
    int in = Serial.parseInt(); //Recebe entrada
    digitalWrite(LED_BUILTIN, in); //Acende ou apaga o led
  }

}
