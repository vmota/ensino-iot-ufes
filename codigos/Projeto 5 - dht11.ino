#include <Adafruit_Sensor.h>


//incluo a biblioteca
#include <DHT.h>



#define DHTTYPE DHT11 

//defino o pino
uint8_t DHTPIN = D1;

DHT dht(DHTPIN, DHTTYPE);

void setup(){
  // inicializo porta serial
  Serial.begin(115200);
  dht.begin();
}

void loop()
{
  //leio temperatura
  float tempC = dht.readTemperature();

  //leio umidade
  float humidity = dht.readHumidity();

  Serial.print("Temperature (C) = ");
  Serial.println(tempC);

  Serial.print("Humidity = ");
  Serial.println(humidity);
  delay(3000);
}