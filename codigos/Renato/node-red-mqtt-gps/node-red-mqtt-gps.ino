#include <TimeLib.h>
#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>

//Pinos utilizados para conexao do modulo GY-NEO6MV2
static const int RXPin = 13, TXPin = 15;

//Objeto TinyGPS++
TinyGPSPlus gps;

//Conexao serial do modulo GPS
SoftwareSerial Serial_GPS(RXPin, TXPin);

//Ajuste o timezone de acordo com a regiao
const int UTC_offset = -3;

// Mude as variáveis a seguir de acordo com a rede que você vai conectar o seu ESP8266
const char* ssid = "LenovoK6";
const char* password = "ringoeperola";

// Aqui você deve modificar de acordo com o servidor que você irá utilizar, será o mesmo endereço que você colocou no nó 'mqtt' no Node-RED
const char* mqtt_server = "broker.mqtt-dashboard.com";

// Inicializa o ESP8266 como cliente
WiFiClient espClient;
PubSubClient client(espClient);

// Função que conecta o ESP8266 à rede
void setup_wifi() {
  delay(10);
  /*Serial.println();
  Serial.print("Conectando-se a ");
  Serial.println(ssid);*/
  WiFi.begin(ssid,password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    //Serial.print(".");
  }
  /*Serial.println("");
  Serial.print("WiFi connectado - endereço IP do ESP8266: ");
  Serial.println(WiFi.localIP());*/
}

// Quando alguma mensagem é publicada em algum tópico que o ESP8266 estiver inscrito esta função é executada,
// portanto ela é alterada de acordo com a lógica que o programador deseja.
void callback(String topic, byte* message, unsigned int length) {
  /*Serial.print("Chegou uma mensagem no tópico: ");
  Serial.print(topic);
  Serial.print(". Mensagem: ");*/
  String messageTemp;
  
  for (int i = 0; i < length; i++) {
    //Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }
  //Serial.println();
  
  // Aqui é checada se a mensagem que chegou foi no tópico 'iotufes/led', se sim checa se a mensagem é 'acende' ou 'apaga'. E realiza a ação.
  /*if(topic=="iotufes/led"){
      if(messageTemp == "acende"){
        Serial.print("Acendendo LED");
        digitalWrite(lamp, HIGH);
      }
      else if(messageTemp == "apaga"){
        Serial.print("Apagando LED");
        digitalWrite(lamp, LOW);
      }
  }*/
  //Serial.println();
}

// Esta função reconecta o ESP8266 ao servidor MQTT
void reconnect() {
  // Fica em loop até conectar novamente
  while (!client.connected()) {
    // Tentativa de conexão. O argumento passado na função 'connect' é o id da conexão MQTT, este deve ser único em um servidor.
    if (client.connect("iotufes")) {
      // Aqui nos increvemos ou reinscrevemos nos tópicos
      // Você pode se inscrever em mais de um tópico com o mesmo ESP8266
      client.subscribe("iotufes/led");
    } else {
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

// O setup simplesmente inicializa o pino D6 como saída e inicia uma comunicação serial com baud rate de 115200
// Essa comunicação serial pode ser utilizada para visualizar as mensagens impressas
// Aqui também é inicializado qual servidor MQTT e a função callback, é ela que recebe as mensagens do MQTT e controla o LED
void setup() {
  
  //Baud rate Modulo GPS
  Serial_GPS.begin(9600);

  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
  GPS_Timezone_Adjust();

}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  if(!client.loop())
    client.connect("iotufes");


  //Conexao com modulo GPS
  while (Serial_GPS.available() > 0){
    if (gps.encode(Serial_GPS.read())){
      client.publish("iotufes/gps","Adquirindo dados");
      displayInfo();
    }
  }

  if (millis() > 5000 && gps.charsProcessed() < 10)
  {
    client.publish("iotufes/gps","No GPS detected: check wiring.");
    while (true);
  }

  GPS_Timezone_Adjust();
  delay(1000*30);
} 

void displayInfo()
{
  //Mostra informacoes no Serial Monitor
  //client.publish("iotufes/gps","Location: ");
  if (gps.location.isValid())
  {
    static char lat[13];
    dtostrf(gps.location.lat(), 10, 6, lat);
    static char lng[13];
    dtostrf(gps.location.lng(), 10, 6, lng);
    static char loc[38];
    sprintf(loc, "Location: %s, %s", lat, lng);
    client.publish("iotufes/gps",loc);
  }
  else
  {
    client.publish("iotufes/gps","Location: INVALID");
  }

  //client.publish("iotufes/gps","Date: ");
  if (gps.date.isValid())
  {
    static char day[3];
    dtostrf(gps.date.day(), 2, 0, day);
    static char month[3];
    dtostrf(gps.date.month(), 2, 0, month);
    static char year[3];
    dtostrf(gps.date.year(), 2, 0, year);
    static char dat[17];
    sprintf(dat, "Date: %s/%s/%s", day, month, year);
    client.publish("iotufes/gps",dat);
  }
  else
  {
    client.publish("iotufes/gps","Date: INVALID");
  }

  //client.publish("iotufes/gps","Time: ");
  if (gps.time.isValid())
  {
    static char hour[3];
    dtostrf(gps.time.hour(), 2, 0, hour);
    static char minute[3];
    dtostrf(gps.time.minute(), 2, 0, minute);
    static char second[3];
    dtostrf(gps.time.second(), 2, 0, second);
    static char centisecond[3];
    dtostrf(gps.time.centisecond(), 2, 0, centisecond);
    static char dat[21];
    sprintf(dat, "Time: %s:%s:%s.%s", hour, minute, second, centisecond);
    client.publish("iotufes/gps",dat);
  }
  else
  {
    client.publish("iotufes/gps","Time: INVALID");
  }
  //client.publish("iotufes/gps","\n");
}

void GPS_Timezone_Adjust()
{
  while (Serial_GPS.available())
  {
    if (gps.encode(Serial_GPS.read()))
    {
      int Year = gps.date.year();
      byte Month = gps.date.month();
      byte Day = gps.date.day();
      byte Hour = gps.time.hour();
      byte Minute = gps.time.minute();
      byte Second = gps.time.second();

      //Ajusta data e hora a partir dos dados do GPS
      setTime(Hour, Minute, Second, Day, Month, Year);
      //Aplica offset para ajustar data e hora
      //de acordo com a timezone
      adjustTime(UTC_offset * SECS_PER_HOUR);
    }
  }
}
