// Define variável para estado do LED
int led_status = 1;

void setup() {
  // Inicializa porta serial com baud rate 115200
  Serial.begin(115200);
  // Inicializa pino D4 (LED interno) como saída
  pinMode(D4, OUTPUT);
  // Apaga o LED
  digitalWrite(D4, led_status);
}

void loop() {
  // Verifica se tem algo para consumir no buffer da porta Serial
  if(Serial.available() > 0){
    // Consome um inteiro do buffer e armazena na variável
    led_status = Serial.parseInt();
    // Seta o pino D4 de acordo com o que foi consumido no buffer. LED interno acende se leu 0 e apaga se leu 1
    digitalWrite(D4, led_status);
  }
}
