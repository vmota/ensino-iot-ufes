#include <SPI.h>
#include <MFRC522.h>

#define SS_PIN D8  //D8
#define RST_PIN D3 //D3

MFRC522 mfrc522(SS_PIN, RST_PIN);   // Cria o objeto MFRC522.
int statuss = 0;
int out = 0;

void setup() 
{
  Serial.begin(9600);   // Inicia a comunicação serial que é usava para imprimir informações no monitor
  SPI.begin();          // Inicia o protocolo SPI
  mfrc522.PCD_Init();   // Inicia o leitor RFID
  Serial.println("\nLeitor iniciado:");
}

void loop() 
{
  // Procura por tags novas
  if ( ! mfrc522.PICC_IsNewCardPresent()) 
  {
    return;
  }
  // Seleciona uma das tags
  if ( ! mfrc522.PICC_ReadCardSerial()) 
  {
    return;
  }
  // Imprime o UID da tag no monitor serial
  Serial.println();
  Serial.print("UID da tag lida:");
  String content= "";
  byte letter;
  for (byte i = 0; i < mfrc522.uid.size; i++) 
  {
     Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
     Serial.print(mfrc522.uid.uidByte[i], HEX);
     content.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
     content.concat(String(mfrc522.uid.uidByte[i], HEX));
  }
  content.toUpperCase();
  Serial.println();
  
  if (content.substring(1) == "4B 92 9C 15") //mude o UID para os que deseja conceder acesso
  {
    Serial.println("Acesso Confirmado");
    Serial.println("Seja bem-vindo visitante");
    delay(1000);
    statuss = 1;
  }
  else   {
    Serial.println("Accesso Negado");
    delay(3000);
  }
  
} 
