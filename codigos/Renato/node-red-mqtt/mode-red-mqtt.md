![alt text](imgs/logo-gitlab.png)

# Acendendo um LED via Web com o Node RED e MQTT
**Nível:** 3/5

**Objetivo:**
Implementar a integração entre o ESP8266 e uma ferramenta Web, no caso o Node-RED, através do protocolo MQTT para fazer operações com o LED via Web.

**Materiais necessários:**
* ESP8266
* Protoboard
* 1 LED (Light Emitting Diode)
* 1 Resistor de 330Ω à 1KΩ
* 2 Cabos Jumper Macho-Macho
* Node-RED instalado em um computador

**Instruções:**

O ESP8266 é uma placa ótima para uso em aplicações IoT (Internet of Things), por isso é interessante que possamos nos comunicar com ela via Web dispensando a necessidade de nos conectar fisicamente a ela para obter dados ou enviar/receber comandos. Entretanto nesse experimento vamos usar a placa conectada ao computador como forma de alimentação e comunicação serial.

Neste tutorial a ferramenta Web que será utilizada é o Node-RED, baseada na programação em fluxos e desenvolvida pela IBM, ela tem seu foco em aplicações IoT, além de ser um dos principais serviços do Bluemix, a plataforma em nuvem mais recente da IBM.

O Node-RED é uma ferramenta que visa fazer essa integração do Hardware com serviços online, possuindo diversos destes disponíveis para uso, como Twitter, Facebook, E-mail e outros, tudo isso com o uso de fluxogramas de fácil visualização. Logo, é possível montar uma infinidade de aplicações com essa ferramenta, desde simples operações usando a GPIO de um Raspberry Pi até aplicações complexas, envolvendo rotinas para análise de redes sociais e envio de mensagens eletrônicas, e claro, podendo ser feito tudo remotamente via web.

Nesse projeto serão feitas operações com LED com o uso do Node-RED, então o primeiro requisito é ter o Node-RED instalado em um computador, na [página oficial do software](https://nodered.org/docs/getting-started/) existem instruções de instalação.

A comunicação entre o ESP8266 e o Node-RED será feita através do protocolo MQTT que é baseado no conceito Publisher/Subscriber. Resumidamente funciona assim, existe uma entidade central chamada Broker e nela os Publishers publicam mensagens e os Subscribers consomem as mensagens, este é um padrão bastante utilizado pois nele há uma independência entre os Publishers e Subscribers, não sendo necessário um saber do outro. A comunicação acontece pois os Subscribers ouvem a tópicos específicos do Broker e os Publishers falam também em tópicos específicos. A figura abaixo ilustra o padrão:

![alt text](imgs/pubsubimg.jpg)

**Etapas:**
1. Vamos começar criando um fluxo no Node-RED, para isso abra o terminal(Linux) ou prompt de comando(Windows) e entre com o comando 'node-red', em seguida digite na barra de endereço do seu navegador 'localhost:1880'.

2. No painel à esquerda arraste para o centro da tela dois nós do tipo 'inject' que estão no grupo 'input'.

3. Dê dois cliques em um deles para editá-lo, no campo 'Payload' escolha a opção 'string' e digite "acende", sem as aspas. Repita no outro nó, só que digite "apaga" no campo 'Payload'.

4. Agora vamos inserir no fluxo um nó do tipo 'mqtt' que está no grupo 'output'. Dê dois cliques nele para editar, no campo 'Topic' digite "iotufes/led".

5. No campo 'Server' selecione a opção 'Add new mqtt-broker' e clique no lápis à direita, aqui vamos usar um servidor remoto de teste, então insira no campo 'Server' o seguinte endereço "broker.mqtt-dashboard.com" e no campo 'Port' insira 1883, e clique em update.
Caso queira usar servidor local, é necessário instalar o protocolo MQTT em seu computador. Para isso, primeiramente, abra o terminal e atualize os pacotes com os seguintes comandos:
```
sudo apt-get update
sudo apt-get upgrade
```
Em seguida, digite o comando para instalação:
```
sudo apt-get install mosquitto
```
Entao, retorne ao node-red e no campo 'Server' insira o endereço IP do seu computador, que pode ser encontrado nas configurações ou digitando o comando abaixo no terminal:
```
/sbin/ifconfig -a | grep broadcast | sed 's/netmask.*//g; s/[^0-9.]//g'
```

6. Por fim, conecte as saídas dos nós 'inject' no nó 'mqtt' e clique em 'deploy', no canto superior direito.

7. Partimos agora para preparar o script no Arduino IDE, caso você não tenha realizado o experimento anterior siga [este roteiro](https://gitlab.com/vmota/ensino-iot-ufes/snippets/1858507) até o passo 3.

8. Agora precisamos de uma biblioteca do Arduino que realize a funcionalidade Publisher/Subscriber e suporte o protocolo MQTT, esta é a [PubSubClient](https://github.com/knolleary/pubsubclient) que você pode baixar [aqui](https://github.com/knolleary/pubsubclient/archive/master.zip).

9. Depois de baixar o arquivo 'master.zip', extraia seu conteúdo e renomeie a pasta 'pubsubclient-master' para "pubsubclient".

10. Agora mova essa pasta para dentro da pasta 'libraries' localizada onde foi instalado o Arduino IDE. No Linux deve estar em /home/username/Arduino/libraries.

11. Execute o Arduino IDE, caso ele estava executando durante o passo 10 será necessário reiniciá-lo.

12. Copie o código, no fim da página, e cole no ArduinoIDE. Ou faça o download a seguir e abra com o Arduino IDE:
[node-red-mqtt.ino](/uploads/a07e28211bdf5b0084e178804da488bd/node-red-mqtt.ino)

13. Faça as alterações descritas nos comentários do código, conecte o ESP8266 ao computador e carregue o script no ESP8266 clicando no botão 'Carregar' do Arduino IDE, é o botão com uma seta para direita.

14. Por fim vamos montar o circuito, desconecte o ESP8266 do computador, isso corta a alimentação de energia da placa o que tira o risco de queimar componentes. Replique o desenho abaixo substituindo as linhas azuis pelos jumpers. Lembrando que a perna mais longa, anodo, do LED é conectada ao ESP8266 na porta D6.

![alt text](imgs/pisca_led_2.png)

15. Tudo preparado para o teste. Conecte o ESP8266 ao computador, caso queira ver as mensagens impressas pelo ESP8266 abra o monitor serial do Arduino IDE na aba 'Ferramentas', deixe a velocidade a mesma do código, ou seja, 115200 (não confundir com 15200).

16. Agora no Node-RED clique nos botões à esquerda dos nós 'inject' para acender e apagar o LED.

17. Pronto, agora você consegue controlar um LED via WEB.