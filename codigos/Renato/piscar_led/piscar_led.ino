int cont = 0;
void setup(){
  pinMode(D4, OUTPUT);      // O LED interno do ESP8266 está conectado ao pino D4 (GPIO2)
}

void loop(){
  while(cont < 5){          // Pisca o LED 5 vezes. Para evitar que fique piscando eternamente
    digitalWrite(D4, LOW);  // Acende o LED, pois no ESP8266 o LED acende quando a saída D4 é baixa(0V)
    delay(1000);            // Espera 1 segundo
    digitalWrite(D4, HIGH); // Apaga o LED
    delay(1000);            // Espera 1 segundo
    cont++;
  }
}
