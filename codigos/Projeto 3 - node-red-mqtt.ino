#include <ESP8266WiFi.h>
#include <PubSubClient.h>

// Mude as variáveis a seguir de acordo com a rede que você vai conectar o seu ESP8266
const char* ssid = "sua_rede";
const char* password = "senha_da_rede";

// Aqui você deve modificar de acordo com o servidor que você irá utilizar, será o mesmo endereço que você colocou no nó 'mqtt' no Node-RED
const char* mqtt_server = "broker.mqtt-dashboard.com";

// Inicializa o ESP8266 como cliente
WiFiClient espClient;
PubSubClient client(espClient);

// No nosso circuito o LED está conectado no pino D6 que é o GPIO12 na placa D1 mini
const int lamp = 12;

// Função que conecta o ESP8266 à rede
void setup_wifi() {
  delay(10);
  Serial.println();
  Serial.print("Conectando-se a ");
  Serial.println(ssid);
  WiFi.begin(ssid,password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("WiFi connectado - endereço IP do ESP8266: ");
  Serial.println(WiFi.localIP());
}

// Quando alguma mensagem é publicada em algum tópico que o ESP8266 estiver inscrito esta função é executada,
// portanto ela é alterada de acordo com a lógica que o programador deseja.
void callback(String topic, byte* message, unsigned int length) {
  Serial.print("Chegou uma mensagem no tópico: ");
  Serial.print(topic);
  Serial.print(". Mensagem: ");
  String messageTemp;
  
  for (int i = 0; i < length; i++) {
    Serial.print((char)message[i]);
    messageTemp += (char)message[i];
  }
  Serial.println();
  
  // Aqui é checada se a mensagem que chegou foi no tópico 'iotufes/led', se sim checa se a mensagem é 'acende' ou 'apaga'. E realiza a ação.
  if(topic=="iotufes/led"){
      if(messageTemp == "acende"){
        Serial.print("Acendendo LED");
        digitalWrite(lamp, HIGH);
      }
      else if(messageTemp == "apaga"){
        Serial.print("Apagando LED");
        digitalWrite(lamp, LOW);
      }
  }
  Serial.println();
}

// Esta função reconecta o ESP8266 ao servidor MQTT
void reconnect() {
  // Fica em loop até conectar novamente
  while (!client.connected()) {
    Serial.print("Tentando conexão MQTT...");
    // Tentativa de conexão. O argumento passado na função 'connect' é o id da conexão MQTT, este deve ser único em um servidor.
    if (client.connect("iotufes")) {
      Serial.println("connectado");
      // Aqui nos increvemos ou reinscrevemos nos tópicos
      // Você pode se inscrever em mais de um tópico com o mesmo ESP8266
      client.subscribe("iotufes/led");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

// O setup simplesmente inicializa o pino D6 como saída e inicia uma comunicação serial com baud rate de 115200
// Essa comunicação serial pode ser utilizada para visualizar as mensagens impressas
// Aqui também é inicializado qual servidor MQTT e a função callback, é ela que recebe as mensagens do MQTT e controla o LED
void setup() {
  pinMode(lamp, OUTPUT);
  digitalWrite(lamp, LOW);
  
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

}

void loop() {

  if (!client.connected()) {
    reconnect();
  }
  if(!client.loop())
    client.connect("iotufes");
} 